﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bayonet : MonoBehaviour {

    Vector3 m_vecInitPos = new Vector3();
    public SpriteRenderer _srMain;
    float m_fTotalDistance = 0.0f;
    float m_fCurMoveDistance = 0.0f;
    public float m_fDir = 1.0f;
    bool m_bIsMoving = false;
    float m_fSpeed = 3.0f;
    int m_nStatus = 0;
    Vector3 vecTempPos = new Vector3();
    public BayonetTunnel _bayoTunnel = null;
    // Use this for initialization
    void Start () {
		
	}

    void Awake()
    {
        Init();
    }

    void Init()
    {
        m_vecInitPos = this.gameObject.transform.localPosition;
        m_fTotalDistance = 2.7f;
    }

    // Update is called once per frame
    void Update () {
        Move();
    }

    public void BeginMove()
    {
        m_bIsMoving = true;
        m_fCurMoveDistance = 0.0f;
        m_nStatus = 1;
        _srMain.material.SetTextureOffset("_Mask", new Vector2(0.0f, -0.5f));
    }

    void Move()
    {
        if (!m_bIsMoving)
        {
            return;
        }

        float fMoveDelta = m_fSpeed * m_fDir * Time.deltaTime;
        m_fCurMoveDistance += fMoveDelta;

        vecTempPos = this.gameObject.transform.localPosition;
        vecTempPos.y += fMoveDelta;
        float fAbsCurMoveDistance = Mathf.Abs(m_fCurMoveDistance);
        this.gameObject.transform.localPosition = vecTempPos;
        if (m_nStatus == 1)
        {
            float fMask = -0.5f + fAbsCurMoveDistance / m_fTotalDistance * 1.0f;
            _srMain.material.SetTextureOffset("_Mask", new Vector2(0.0f, fMask));
            if (fAbsCurMoveDistance >=  m_fTotalDistance )
            {
                m_fCurMoveDistance = 0.0f;
                m_fDir = -m_fDir;
                m_nStatus = 2;

            }
        }
        else if ( m_nStatus == 2 )
        {
            float fMask = 0.5f - fAbsCurMoveDistance / m_fTotalDistance * 1.0f;
            _srMain.material.SetTextureOffset("_Mask", new Vector2(0.0f, fMask));
            if (fAbsCurMoveDistance >= m_fTotalDistance )
            {
                m_fCurMoveDistance = 0.0f;
                m_fDir = -m_fDir;
                m_nStatus = 0;
                EndMove();
            }
        }
    }

    void EndMove()
    {
        m_bIsMoving = false;
    }

    public bool IsMoving()
    {
        return m_bIsMoving;
    }
}
