﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class UI_Spit : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    Main m_Main;
	public static bool s_bUsingUi = false;

	// Use this for initialization
	void Start ()
    {
        GameObject go;
        go = GameObject.Find("Main Camera");
        m_Main = go.GetComponent<Main>();
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void OnPointerDown(PointerEventData evt)
	{
		if (this.gameObject.name == "btnSpit") {
			m_Main.BeginSpit ();
			s_bUsingUi = true;
		}
    }

    public void OnPointerUp(PointerEventData evt)
    {
		if (this.gameObject.name == "btnSpit") {	
			m_Main.EndSpit ();
			s_bUsingUi = false;
		}
    }

}
