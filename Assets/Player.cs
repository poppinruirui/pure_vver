﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Photon.PunBehaviour , IPunObservable 
{
	public bool _isRebornProtected = true;

	public Color _color_inner;
	public Color _color_ring;
	public Color _color_poison;

	float m_fProtectTime= 0.0f;

	// Use this for initialization
	void Start () 
	{
		
	}

	void Awake()
	{
		Init ();
	}
	
	// Update is called once per frame
	void Update () {
		ProtectTimeCount ();

	}

	void Init()
	{
		_isRebornProtected = true; // 第一次出生时一定是处于“重生保护状态”

		this.gameObject.name = "player_" + photonView.ownerId;
		this.gameObject.transform.parent = Main.s_Instance.m_goBalls.transform;

		int color_index = 0;
		_color_ring = ColorPalette.RandomOuterRingColor (ref color_index);
		_color_inner = ColorPalette.RandomInnerFillColor ( photonView.ownerId/*color_index*/ );
		_color_poison = ColorPalette.RandomPoisonFillColor ( photonView.ownerId/*color_index*/);
	}

	public void PhotonRemoveObject( GameObject go )
	{
		PhotonNetwork.Destroy( go );
	}

	public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{

	}

	public void BeginProtect()
	{
		photonView.RPC ( "RPC_BeginProtect", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_BeginProtect()
	{
		m_fProtectTime = 5.0f;
	}

	void ProtectTimeCount()
	{
		if (m_fProtectTime > 0.0f) {
			m_fProtectTime -= Time.deltaTime;
		}
	}

	public bool IsProtect()
	{
		return m_fProtectTime > 0.0f;
	}

}
