﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	// 接触
	void OnCollisionEnter2D(Collision2D other)
	{
		ProcessCollision (other);
	}
		
	// 胶着状态(继续PK)
	void OnCollisionStay2D(Collision2D other)
	{
		ProcessCollision (other);
	}

	void ProcessCollision( Collision2D other )
	{
		/*
		Debug.Log ( "111111: " + this.transform.gameObject.name );
		if (this.transform.gameObject.name == "ColliderForNail" ) {
			Collider2D this_collider = this.transform.gameObject.GetComponent<Collider2D> ();
			Nail nail = other.transform.gameObject.GetComponent<Nail> ();
			if (nail == null) {
				Collider2D other_collier = other.transform.gameObject.GetComponent<Collider2D> (); 
				Physics2D.IgnoreCollision ( this_collider, other_collier);
				return;
			}
		}
		*/


		Ball ballSelf = this.transform.parent.gameObject.GetComponent<Ball> ();  
		Ball ballOpponet = other.transform.gameObject.GetComponent<Ball> (); // Collision不在shell上，居然在Ball上
		if (ballOpponet && ballSelf) {
			if (ballOpponet._Player.photonView.ownerId != ballSelf._Player.photonView.ownerId) {
				Collider2D collider_this = this.transform.gameObject.GetComponent<Collider2D> ();
				Collider2D collider_other = other.transform.gameObject.GetComponent<Collider2D> ();
				Physics2D.IgnoreCollision (ballSelf._Collider, ballOpponet._Collider);
			}
			Physics2D.IgnoreCollision (ballSelf._ColliderForNail, ballOpponet._ColliderForNail);
			Physics2D.IgnoreCollision (ballSelf._ColliderForNail, ballOpponet._Collider);
		}

	}
	
	// 分离
	void OnCollisionExit2D(Collision2D coll)
	{
/*		Ball ballOpponet = coll.transform.parent.gameObject.GetComponent<Ball> ();
		Ball ballSelf = this.transform.parent.gameObject.GetComponent<Ball> ();
		ballSelf.SetPkOppoent( null );
		ballOpponet.SetPkOppoent( null );
*/

	}
}
