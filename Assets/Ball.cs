using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : Photon.PunBehaviour , IPunObservable 
{
	public GameObject _MainForAlpha;
	public GameObject _shell; 
	public GameObject _shellForAlpha;
	public CircleCollider2D _collider;
	public GameObject _thorn;
	public GameObject _thornForAlpha;
	public GameObject _trigger;
	public SpriteRenderer _srMain;
	public CircleCollider2D _Trigger;
	public CircleCollider2D _Collider;
	public CircleCollider2D _ColliderForNail;
	public GameObject _Mouth;
	public SpriteRenderer _srMouth;
	public GameObject _main; // 暂不废弃
	public Rigidbody2D _rigid;

	public SpriteRenderer _srMainForAlpha;
	public SpriteRenderer _srThornForAlpha;
	public SpriteRenderer _srShellForAlpha;

	bool m_bIsEjecting = false;

	public Vector3 vecTempSize = new Vector3();
    public Vector3 vecTempPos = new Vector3 ();
    public Vector3 vecTempPos2 = new Vector3(); 
    public Vector3 vecTempScale = new Vector3 ();
    public Color cTempColor = new Color();

	Vector2 _direction = new Vector2();

	public TextMesh _text;

    public BayonetTunnel _bayoTunnel = null;

    //// MiniMap 相关
    MiniMapBall m_MyMiniMapDouble = null;
    public MiniMapBall GetMyMiniMapDouble()
    {
        return m_MyMiniMapDouble;
    }

    public void SetMiniMapDouble(MiniMapBall obj )
    {
        m_MyMiniMapDouble = obj; 
    }

    float m_fDirX = 0.0f;
	float m_fDirY = 0.0f;
 	public float _dirx
	{
		set {  
			if (_ba == null) {
				_direction.x = value;
			} else {
				_ba._direction.x = value; 
			}

		}
		get { 
			if (_ba == null) {
				return _direction.x;
			} else {
				return _ba._direction.x; 
			}
		}
	}

	public float _diry
	{
		set {  
			if (_ba == null) {
				_direction.y = value;
			} else {
				_ba._direction.y = value; 
			}

		}
		get { 
			if (_ba == null) {
				return _direction.y;
			} else {
				return _ba._direction.y; 
			}
		}
	}
   
	public ObsceneMaskShader _obsceneCircle;
	float m_fSpeed = 0.0f;
    float m_fTargetSize = 1.0f;

	public float _speed
	{
		get { 
				return m_fSpeed;
		}

		set{
			m_fSpeed = value;

		}
	}

	public void SetDirX( float val )
	{
		photonView.RPC ( "RPC_SetDirX", PhotonTargets.All, val );
	}

	[PunRPC]
	public void RPC_SetDirX( float val )
	{
		_dirx = val;
	}

	public void SetDirY( float val )
	{
		photonView.RPC ( "RPC_SetDirY", PhotonTargets.All, val );
	}

	[PunRPC]
	public void RPC_SetDirY( float val )
	{
		_diry = val;
	}

	public void SetSpeed( float fSpeed )
	{
		photonView.RPC ( "RPC_SetSpeed", PhotonTargets.All, fSpeed );
	}

	[PunRPC]
	public void RPC_SetSpeed( float fSpeed )
	{
		_speed = fSpeed;
	}

	public Player _Player = null; // 本Ball所属的Player
	public GameObject _player = null; // 这个 ball所属的player
				
	public int ownerId
	{
		get { return photonView.ownerId;  }
	}

	PhotonTransformView m_PhoTransView;

	eBallType m_eBallType = eBallType.ball_type_ball;
	public eBallType _balltype
	{
		set {  m_eBallType = value; }
		get {  return m_eBallType; }
	}

	public enum eBallType
	{
		ball_type_ball,          // 常规的球
		ball_type_thorn,         // 刺
		ball_type_bean,          // 豆子
		ball_type_spore,         // 孢子 
	};

	public BallAction _ba; // 小熊那边做的BallAction模块

	public void OnPhotonSerializeView (PhotonStream stream, PhotonMessageInfo info)
	{
		// poppin to do：这里明显需要优化：很多属性是不需要频繁同步的（对方客户端完全可以自己运算）。
		if(stream.isWriting) // MainPlayer
		{
			stream.SendNext(GetSize());
			stream.SendNext(GetThornSize());

			stream.SendNext ( _speed );
			stream.SendNext ( _dirx );
			stream.SendNext ( _diry );

            stream.SendNext(m_fShellShrinkCurTime);
        }

		else // Other
		{
			Local_SetSize ((float)stream.ReceiveNext ());
			Local_SetThornSize ((float)stream.ReceiveNext ());
			_speed = (float)stream.ReceiveNext ();
			_dirx = (float)stream.ReceiveNext ();
			_diry = (float)stream.ReceiveNext ();

            m_fShellShrinkCurTime = (float)stream.ReceiveNext(); // 现在是核心玩法迭代器，不用太考虑性能，除非性能已经影响了Demo效果

            /*
			float fSize = (float)stream.ReceiveNext ();
			float fInnerSize = (float)stream.ReceiveNext ();
			bool bIsInnerBallGrowing = (bool)stream.ReceiveNext ();
			speed = (float)stream.ReceiveNext ();

			SetSize (fSize);
			SetInnerBallSize ( fInnerSize );

			SetIfInnerBallGrowing ( bIsInnerBallGrowing );


			this.gameObject.SetActive (true);
			*/
        }	
	}


	public void DestroyBall()
	{
        
        //	photonView.RPC ( "RPC_DestroyBall", PhotonTargets.All );
        if (photonView.isMine) {
            //this.Die();
            Main.s_Instance.PhotonDestroy (this.gameObject);
		} 
	}

	/*
	[PunRPC]
	public void RPC_DestroyBall()
	{
		if (photonView.isMine) {
			Main.s_Instance.PhotonDestroy (this.gameObject);
		} 
	}
	*/

	bool m_bDead = false;
	public void Die()
	{
        /*
		m_bDead = true;
		
		if (_relateTarget) {
			DestroyTarget ( _relateTarget );
		}

		gameObject.SetActive ( false );

		if (_Trigger) {
			_Trigger.enabled = false;
		}

		if (_Collider) {
			_Collider.enabled = false;
		}
        */
	}

	public bool IsDead()
	{
		return m_bDead;
	}

	// [poppin youhua]
	public static void DestroyTarget( SpitBallTarget target )
	{
		GameObject.Destroy ( target.gameObject );
	}

	// [poppin youhua]
	public static void DestroyBean( Bean bean )
	{
		bean.gameObject.SetActive ( false );
		GameObject.Destroy ( bean.gameObject );
		if (bean._Trigger) {
			bean._Trigger.enabled = false;
		}

        if (bean._beanType == Bean.eBeanType.scene)
        {
            Bean.ApplyGenerateOneNewBean();
        }
	}

	// [poppin youhua]
	public static void DestroyThorn( Thorn thorn )
	{
		thorn.gameObject.SetActive ( false );
		GameObject.Destroy ( thorn.gameObject );
		if (thorn._Trigger) {
			thorn._Trigger.enabled = false;
		}
		Thorn.ApplyGenerateOneNewThorn ();
	}

	SpriteRenderer m_sr;
	void Awake()
	{
		Init ();

		m_sr = this.gameObject.GetComponent<SpriteRenderer> () ;

	}

    void Start()
    {
        
    }

    public virtual void Init()
	{
		GameObject go;


			if (photonView.isMine) 
			{
				_thornForAlpha.SetActive (true);
				_MainForAlpha.SetActive (true);
				_shellForAlpha.SetActive (true);
			}
			else
			{
				_thornForAlpha.SetActive( false );
				_MainForAlpha.SetActive (false);
			    _shellForAlpha.SetActive (false);
			}

        _srMain = this.gameObject.GetComponent<SpriteRenderer>();
        _player = GameObject.Find ("Balls/player_" + photonView.ownerId);
        if (_player)
        {
            _Player = _player.GetComponent<Player>();
            this.gameObject.transform.parent = _player.transform;

            _srMain.color = _Player._color_inner;
            if (photonView.isMine)
            {
                DarkForest.s_Instance.AddBall(this);
            }

            MiniMap.s_Instance.AddBall(this, photonView.isMine);
        }

		cTempColor =  _srMain.color;
		cTempColor.a = 0.4f;
		_srMainForAlpha.color = cTempColor;


		Vector3 vecSize = new Vector3 ();
		vecSize.x = 10;
		vecSize.y = 15;

		Transform transShell = this.gameObject.transform.FindChild ("Shell"); 
		if (transShell) {
			_shell = transShell.gameObject;
			SpriteRenderer sr = _shell.GetComponent<SpriteRenderer> ();
            if (_Player)
            {
                sr.color = _Player._color_ring;
            }
			cTempColor = sr.color;
			cTempColor.a = 0.4f;
			_srShellForAlpha.color = cTempColor;

		} else {
			Debug.LogError ( "transShell empty" );
			return;
		}
		_Collider = _shell.GetComponent<CircleCollider2D> ();
        _Collider.isTrigger = true;

        Transform transColliderForNail = this.gameObject.transform.FindChild ("ColliderForNail"); 
		if (transColliderForNail) {
			_ColliderForNail = transColliderForNail.GetComponent<CircleCollider2D> ();
		}


		Transform transThorn = this.gameObject.transform.FindChild ("Thorn"); 
		if (transThorn) {
			_thorn = transThorn.gameObject;
			SpriteRenderer sr = _thorn.GetComponent<SpriteRenderer> ();
            if (_Player)
            {
                sr.color = _Player._color_poison;
            }
			transThorn.localScale = Vector3.zero;
			cTempColor =  sr.color;
			cTempColor.a = 0.4f;
			_srThornForAlpha.color = cTempColor;

		} else {
			Debug.LogError ( "transThorn empty" );
			return;
		}

		Transform transMouth = this.gameObject.transform.FindChild ("Mouth"); 
		if (transMouth) {
			_Mouth = transMouth.gameObject;
			_srMouth = _Mouth.GetComponent<SpriteRenderer> ();
            if (_Player)
            {
                _srMouth.color = _Player._color_inner;
            }
		} else {
			Debug.LogError ( "transMouth empty" );
			return;
		}

		_rigid = this.gameObject.GetComponent<Rigidbody2D> ();
			
		_Trigger = this.gameObject.GetComponent<CircleCollider2D> ();;// _Mouth.GetComponent<CircleCollider2D> ();
		_Trigger.isTrigger = true;
		cTempColor = _srMouth.color;
		cTempColor.a = 0.0f;
		_srMouth.color = cTempColor;



		m_PhoTransView = GetComponent<PhotonTransformView> ();

		GameObject goObsceneMask = this.gameObject.transform.FindChild ( "ObsceneCircle" ).gameObject;
		_obsceneCircle = goObsceneMask.GetComponent<ObsceneMaskShader> ();
        if (Launcher.m_eSceneMode == Launcher.eSceneMode.Game)
        {
            vecTempScale.x = Main.s_Instance.m_fUnfoldCircleSale;
            vecTempScale.y = Main.s_Instance.m_fUnfoldCircleSale;
            vecTempScale.z = 1.0f;
            _obsceneCircle.transform.localScale = vecTempScale;
        }
        else
        {
            goObsceneMask.SetActive( false );
        }
		_ba = this.gameObject.GetComponent<BallAction> ();

		go = this.gameObject.transform.FindChild ( "Text" ).gameObject;
		_text = go.GetComponent<TextMesh> ();

		BallSizeChange ();
	}

	// Update is called once per frame
	public virtual void Update () 
    {
		PreUnfolding ();
		Unfolding ();
		Shell ();
        ProcessEatenNode();

        Sync();
    }

    void Sync()
    {
        if ( !m_bShell )
        {
            if ( m_fShellShrinkCurTime > 0.0f )
            {
                Local_BeginShell();
            }
        }
    }

	public virtual void  FixedUpdate()
    {
		Ejecting ();
    }

	public void SetSize( float val )
	{
		photonView.RPC ( "RPC_SetSize", PhotonTargets.All, val );
	}

	[PunRPC]
	public void RPC_SetSize( float val )
	{
		Local_SetSize (val);
	}

	public void Local_SetSize( float val )
	{
		vecTempSize.x = val;
		vecTempSize.y = val;
		vecTempSize.z = 1.0f;
		this.transform.localScale = vecTempSize;

		BallSizeChange ();
	}

	

	void BallSizeChange()
	{
		SetThornSize ( GetThornSize() );
		ShowSize ();

		CalculateMassBySize ( GetSize() );
	}

	void CalculateMassBySize( float fSize )
	{
		if (_rigid == null) {
			_rigid = this.gameObject.GetComponent<Rigidbody2D> ();
		}
		_rigid.mass = fSize * fSize * fSize;
	}

	public float GetSize()
	{
		return this.transform.localScale.x;
	}

	public bool IsEjecting()
	{
		return m_bIsEjecting;
	}

	float m_fEjectInitialSpeed = 0.0f;
	float m_fEjectSpeed = 0.0f;
	float m_fEjectAccelerate = 0.0f;
	float m_fStartPosX = 0.0f;
	float m_fStartPosY = 0.0f;
	float m_fTimeClapse = 0.0f;
	public void BeginEject( float fInitialSpeed, float fAccelerate  )
	{
		photonView.RPC ( "RPC_BeginEject", PhotonTargets.All, fInitialSpeed, fAccelerate );
	}

	[PunRPC]
	public void RPC_BeginEject( float fInitialSpeed, float fAccelerate  )
	{
		Local_BeginEject ( fInitialSpeed, fAccelerate );
	}

	public void Local_BeginEject( float fInitialSpeed, float fAccelerate  )
	{
		m_bIsEjecting = true;
		m_fEjectInitialSpeed = fInitialSpeed;
		m_fEjectSpeed = fInitialSpeed;
		m_fEjectAccelerate = fAccelerate;
		if (_Collider) {
			_Collider.isTrigger = true;
		}

		m_fStartPosX = this.gameObject.transform.position.x;
		m_fStartPosY = this.gameObject.transform.position.y;
		m_fTimeClapse = 0.0f;
	}

	public void EndEject()
	{
		m_bIsEjecting = false;
		if (m_bShell) {
			if (_Collider) {
				_Collider.isTrigger = false;
			}
		}

		if (_relateTarget) {
			DestroyTarget ( _relateTarget );
		}
	}



	void Ejecting()
	{
		if (!m_bIsEjecting) {
			return;
		}

		if (m_fTimeClapse >= Main.s_Instance.m_fSpitRunTime) {
			EndEject ();
			return;
		}

		m_fTimeClapse += Time.fixedDeltaTime;
		vecTempPos.x = m_fStartPosX;
		vecTempPos.y = m_fStartPosY;
		vecTempPos.z = this.gameObject.transform.position.z;
		float S = CyberTreeMath.CalculateDistance( m_fEjectInitialSpeed, m_fEjectAccelerate, m_fTimeClapse ); //m_fEjectSpeed * m_fTimeClapse + 0.5f * m_fAccelerate * m_fTimeClapse * m_fTimeClapse;
		vecTempPos.x += S * _dirx;
		vecTempPos.y += S * _diry;
        bool bRet = CyberTreeMath.ClampBallPosWithinSCreen(vecTempPos, ref vecTempPos2); // right here
		this.gameObject.transform.position = vecTempPos2;
        if ( bRet )
        {
            EndEject();
        }
	}

	int m_nUnfoldStatus = 0;
	public bool CheckIfCanUnfold()
	{
		if (m_nUnfoldStatus > 0) {
			return false;
		}

		return true;
	}

	public void BeginPreUnfold()
	{
		//_obsceneCircle.Begin ( 2.0f );
		//m_nUnfoldStatus = 1;
		photonView.RPC ( "RPC_BeginPreUnfold", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_BeginPreUnfold()
	{
		_obsceneCircle.Begin ( 2.0f );
		m_nUnfoldStatus = 1;
	}

	void PreUnfolding()
	{
		if (m_nUnfoldStatus != 1) {
			return;
		}

		if (_obsceneCircle.IsEnd ()) {
			BeginUnfold ();
		}
	}

	float m_fUnfoldLeftTime = 0.0f;
	void BeginUnfold()
	{
		photonView.RPC ( "RPC_BeginUnfold", PhotonTargets.All );
		/*
		m_nUnfoldStatus = 2;
		m_fUnfoldLeftTime = Main.s_Instance.m_fMainTriggerUnfoldTime;
		vecTempSize.x = Main.s_Instance.m_fUnfoldSale;
		vecTempSize.y = Main.s_Instance.m_fUnfoldSale;
		vecTempSize.z = 1.0f;
		_Mouth.transform.localScale = vecTempSize;
		_Trigger.radius = 1.0f;
		cTempColor = _srMouth.color;
		cTempColor.a = 1.0f;
		_srMouth.color = cTempColor;
		*/
	}

	[PunRPC]
	void RPC_BeginUnfold()
	{
		m_nUnfoldStatus = 2;
		m_fUnfoldLeftTime = Main.s_Instance.m_fMainTriggerUnfoldTime;
		vecTempSize.x = Main.s_Instance.m_fUnfoldSale;
		vecTempSize.y = Main.s_Instance.m_fUnfoldSale;
		vecTempSize.z = 1.0f;
		_Mouth.transform.localScale = vecTempSize;
		_Trigger.radius = 0.5f * Main.s_Instance.m_fUnfoldSale;
		cTempColor = _srMouth.color;
		cTempColor.a = 1.0f;
		_srMouth.color = cTempColor;
	}


	void Unfolding()
	{
		if (m_nUnfoldStatus != 2) {
			return;
		}

		m_fUnfoldLeftTime -= Time.deltaTime;
		if (m_fUnfoldLeftTime <= 0.0f) {
			EndUnfold ();
		}

	}

	void EndUnfold()
	{
		m_nUnfoldStatus = 0;
		vecTempSize = Vector3.one;
		_Mouth.transform.localScale = vecTempSize;
		cTempColor = _srMouth.color;
		cTempColor.a = 0.0f;
		_srMouth.color = cTempColor;
		_Trigger.radius = 0.5f;
	}


	public void ProcessNail( Nail nail )
	{
		if (this.IsEjecting ()) {
			this.EndEject ();
		}

		this.gameObject.transform.parent = Main.s_Instance.m_goBallsBeNailed.transform;
		_Collider.enabled = false;
	}

	public void ProcessPk( Ball ballOpponent )
	{
		if (IsDead () || ballOpponent.IsDead ()) {
			return;
		}

		if ( ( this._Player != null && this._Player.IsProtect () ) || ( ballOpponent._Player != null && ballOpponent._Player.IsProtect () )) {
			return;
		}

		//Debug.Log ( this.name + " , " + ballOpponent.gameObject.name );
		int nWhoIsBig = WhoIsBig( this, ballOpponent );
		if (nWhoIsBig == 0) { // 平手
			return;
		}

		if (nWhoIsBig == 1) {
			this.PreEat (ballOpponent);
		}

		//} else if (nWhoIsBig == -1) {
		//	ballOpponent.PreEat ( this );
		//}
	}

	void PreEat( Ball ballOpponent )
	{
		if (this._balltype != eBallType.ball_type_ball) {
			return;
		}

		if (!CheckIfTotallyCover ( this._Trigger, ballOpponent._Trigger )) {
			return;
		}

		this.Eat (ballOpponent);
	}

	void Eat( Ball ballOpponent )
	{
		if (ballOpponent._balltype == eBallType.ball_type_bean) {
			EatBean ( (Bean)ballOpponent );
			return;
		}

		if (ballOpponent._balltype == eBallType.ball_type_spore) {
			EatSpore ( (Spore)ballOpponent );
			return;
		}

        // “刺”不再继承于“球”，因为最新的设计是把刺归于“场景物件”类，而不是“球”类。
        // 因此“吃刺”的环节也独立出来，不走“吃球”流程
        /*
		if (ballOpponent._balltype == eBallType.ball_type_thorn) {
			EatThorn ( (Thorn)ballOpponent );
			return;
		}
        */

		EatBall ( ballOpponent );
	}

	void EatBean( Bean bean )
	{
  		//SetSize ( Main.s_Instance.m_fBeanSize + GetSize() );
		float fCurSize = GetSize();
		float fBeanSize = Main.s_Instance.m_fBeanSize;
		float fNewSize = 0.0f;

		fNewSize = CyberTreeMath.CalculateNewSize (fCurSize, fBeanSize, Main.s_Instance.m_nShit  );
		SetSize ( fNewSize );

		DestroyBean ( bean );
	}

    Thorn.ColorThornParam m_paramColorThorn = new Thorn.ColorThornParam();
    public void EatThorn( Thorn thorn )
	{
		if (GetSize () < GetMinExplodeSize() || GetSize ()  < Main.s_Instance.m_fMinDiameterToEatThorn ) {
			return;
		}

		float fCurThornSize = GetThornSize ();
        int nColorIdx = thorn.GetColorIdx();
        m_paramColorThorn = MapEditor.GetColorThornParamByIdx(nColorIdx);
        float fAddedThornSize = Main.s_Instance.m_fThornSize;
        if (nColorIdx > 0)
        {
            fAddedThornSize = m_paramColorThorn.thorn_size;
        }

		SetThornSize ( CyberTreeMath.CalculateNewSize ( fCurThornSize, fAddedThornSize, Main.s_Instance.m_nShit ));
		DestroyThorn ( thorn );

//		if (Main.s_Instance.GetCurAvailableBallCount () <= 0) {
//			return;
//		}

		// 吃刺也会增加球本身的总体积。但如果当前队伍已经不能爆刺了，则吃刺不增加球的体积
		float fCurBallSize = GetSize();
		float fAddedBallSize = Main.s_Instance.m_fThornContributeToBallSize;
        if (nColorIdx > 0)
        {
            fAddedBallSize = m_paramColorThorn.ball_size;
        }
        SetSize( CyberTreeMath.CalculateNewSize ( fCurBallSize, fAddedBallSize, Main.s_Instance.m_nShit ) );
	}

	public void SetThornSize( float fThornSize )
	{
		if (!photonView.isMine) {
			return;
		}

		photonView.RPC ( "RPC_SetThornSize", PhotonTargets.All, fThornSize );
	}

	[PunRPC]
	public void RPC_SetThornSize( float fThornSize )
	{
		Local_SetThornSize (fThornSize);
	}

	float m_fThornSize = 0.0f;
	public void Local_SetThornSize( float fThornSize )
	{
		if (_thorn == null) {
			return;
		}

		m_fThornSize = fThornSize;
		float fBallSize = GetSize ();
		if (m_fThornSize > fBallSize) {
			m_fThornSize = fBallSize;
		}
		float fThornRealScaleSize = m_fThornSize / fBallSize;
		vecTempScale.x = fThornRealScaleSize;
		vecTempScale.y = fThornRealScaleSize;
		vecTempScale.z = 1.0f;
		_thorn.transform.localScale = vecTempScale;
		_thornForAlpha.transform.localScale = vecTempScale;
		if (CheckIfExplode ()) {
			Explode ();
		}
	}

	public float GetThornSize()
	{
		return m_fThornSize;
	}

	bool  CheckIfExplode()
	{
		float fBallSize = GetSize ();
		if (m_fThornSize < fBallSize ) {
			return false;
		}

		if (fBallSize < 2.0f) {
			return false;
		}

		return true;
	}

	public static Vector2[] s_aryBallExplodeDirection = { 
		new Vector2( 0.0f, 1.0f ),
		new Vector2( 0.707f, 0.707f ),
		new Vector2( 1.0f, 0.0f ),
		new Vector2( 0.707f, -0.707f ),
		new Vector2( 0, -1.0f ),
		new Vector2( -0.707f, -0.707f ),
		new Vector2( -1.0f, 0.0f ),
		new Vector2( -0.707f, 0.707f ),

	};

	/*
	 * 爆裂的规则：
	 * 1、准备爆裂的母球，半径最小为2。不然没法爆，因为爆出的小球最小半径得是1（一个孢子的尺寸），并且爆了之后母球剩下的半径至少也得是1.
	 * 2、母球分一半的半径出来爆。
	 * 3、能爆成8等分尽量8等分（确保爆出来的每个小球的半径大于等于1）；不够8等分则能爆几个算几个。
	 * */
	public float GetMinExplodeSize()
	{
		return 2.0f * Main.BALL_MIN_SIZE;
	}

    float m_fShitExplodeTime = 0.0f;
	void Explode()
	{
		if (!photonView.isMine) {
			return;
		}

		float fBallSize = GetSize ();
		if (fBallSize < GetMinExplodeSize() ) {
			Debug.LogError ("有Bug, 母球直径小于2肿么爆");
			return;
		}

		/*
		float fChildTotalSize = fBallSize / 2.0f; // 母球分一半的半径出来爆
		float fChildSize = 0.0f;
		int nExpectNum = 8; // 预期分为8份，如果不够8份，则能分几个算几个
		int nRealNum = 0;
		for ( nRealNum = nExpectNum; nRealNum >= 1; nRealNum--) {
			fChildSize = fChildTotalSize / nRealNum;
			if (fChildSize >= Main.BALL_MIN_SIZE) {
				break;
			}
		}
		*/

		int nAvailableNum = (int)Main.s_Instance.m_fMaxBallNumPerPlayer - Main.s_Instance.GetMainPlayerCurBallCount ();
		if (nAvailableNum <= 0) {
			return;
		}

		SetThornSize (0.0f);

		float fChildsArea = CyberTreeMath.SizeToArea( fBallSize, Main.s_Instance.m_nShit ) / 2.0f; // 母球分一半的面积出来爆
		int nExpectNum =  (int)Main.s_Instance.m_fExpectExplodeNum;; // 预期分为8份，如果不够8份，则能分几个算几个
		int nRealNum = 0;
		float fChildSize = 0.0f;
		if (nExpectNum > nAvailableNum) {
			nExpectNum = nAvailableNum;
		}
		for (nRealNum = nExpectNum; nRealNum >= 1; nRealNum--) {
			fChildSize = CyberTreeMath.AreaToSize (fChildsArea / nRealNum, Main.s_Instance.m_nShit);
			if (fChildSize >= Main.BALL_MIN_SIZE) {
				break;
			}
		}

		if (nRealNum == 0) {
			Debug.LogError ("有Bug, 没计算出合适的分球个数");
			return;
		}

		// 生成小球
		float fRadiusMother = GetBoundsSize() / 2.0f;
		vecTempPos = this.gameObject.transform.position;
		for (int i = 0; i < nRealNum; i++) {
            /*
            Main.s_Instance.s_Params [0] = Main.eInstantiateType.Ball;
			Main.s_Instance.s_Params [1] = 222;
			GameObject goNewBall = Main.s_Instance.PhotonInstantiate( Main.s_Instance.m_preBall, vecTempPos, Main.s_Instance.s_Params ) ;//GameObject.Instantiate ( Main.s_Instance.m_preBall );
			Ball new_ball = goNewBall.GetComponent<Ball> ();
			goNewBall.transform.position = vecTempPos;
			new_ball.SetSize ( fChildSize );
			float fRadiusChild = new_ball.GetBoundsSize ();
			new_ball.gameObject.transform.parent = _player.transform;
			Vector2 vecDire = s_aryBallExplodeDirection [i];
			this.CalculateNewBallBornPosAndRunDire ( new_ball, vecDire.x, vecDire.y );
			new_ball._dirx = vecDire.x;
			new_ball._diry = vecDire.y;
			new_ball.BeginShell ();
			CalculateSpitBallRunParams ( fRadiusMother, fRadiusChild, Main.s_Instance.m_fExplodeRunDistanceMultiple, ref m_fExplodeInitSpeed, ref m_fExplodeInitAccelerate );
			new_ball.BeginEject ( m_fExplodeInitSpeed, m_fExplodeInitAccelerate );
            */
            GenerateNewBall(fChildSize, fRadiusMother, i);

        }



		// 母球剩余面积
		SetSize( fBallSize / 1.414f );
		BeginShell ();


	} // end explode()

    void GenerateNewBall( float fChildSize, float fRadiusMother, int idx)
    {
        Main.s_Instance.s_Params[0] = Main.eInstantiateType.Ball;
        Main.s_Instance.s_Params[1] = 222;
        GameObject goNewBall = Main.s_Instance.PhotonInstantiate(Main.s_Instance.m_preBall, vecTempPos, Main.s_Instance.s_Params);//GameObject.Instantiate ( Main.s_Instance.m_preBall );
        Ball new_ball = goNewBall.GetComponent<Ball>();
        goNewBall.transform.position = vecTempPos;
        new_ball.SetSize(fChildSize);
        float fRadiusChild = new_ball.GetBoundsSize();
        new_ball.gameObject.transform.parent = _player.transform;
        Vector2 vecDire = s_aryBallExplodeDirection[idx];
        this.CalculateNewBallBornPosAndRunDire(new_ball, vecDire.x, vecDire.y);
        new_ball._dirx = vecDire.x;
        new_ball._diry = vecDire.y;
        new_ball.BeginShell();
        CalculateSpitBallRunParams(fRadiusMother, fRadiusChild, Main.s_Instance.m_fExplodeRunDistanceMultiple, ref m_fExplodeInitSpeed, ref m_fExplodeInitAccelerate);
        new_ball.BeginEject(m_fExplodeInitSpeed, m_fExplodeInitAccelerate);

    }

    void EatSpore( Spore spore )
	{
		float fChildSize = Main.BALL_MIN_SIZE;
		float fMotherSize = GetSize ();
		SetSize ( CyberTreeMath.CalculateNewSize( fMotherSize, fChildSize, Main.s_Instance.m_nShit ) );

		spore.DestroyBall();
	}

    public struct EatNode
    {
        public Ball eatenBall;
        public float eatenSize; 
    };

    List<EatNode> m_lstEaten = new List<EatNode>();

    void AddEatenNode(EatNode node)
    {
        m_lstEaten.Add( node );
    }

    void ProcessEatenNode()
    {
        for ( int i = m_lstEaten.Count - 1; i >= 0; i--  )
        {
            EatNode node = m_lstEaten[i];
            if (node.eatenBall == null)
            {
                float fMotherSize = GetSize();
                float fNewSize = CyberTreeMath.CalculateNewSize(fMotherSize, node.eatenSize, Main.s_Instance.m_nShit);
                SetSize(fNewSize);
                m_lstEaten.Remove( node );
            }
        }
    }

    bool CheckIfInEatenList(Ball ballOpponent)
    {
        for (int i = m_lstEaten.Count - 1; i >= 0; i--)
        {
            EatNode node = m_lstEaten[i];
            if (node.eatenBall == ballOpponent)
            {
                return true;
            }
        }

        return false;
    }

    void EatBall( Ball ballOpponent )
	{
        if (CheckIfInEatenList(ballOpponent))
        {
            return;
        }

		float fChildSize = ballOpponent.GetSize ();
		float fMotherSize = GetSize ();

        EatNode node = new EatNode();
        node.eatenSize = fChildSize;
        node.eatenBall = ballOpponent;
        AddEatenNode(node);
        //float fNewSize = CyberTreeMath.CalculateNewSize(fMotherSize, fChildSize, Main.s_Instance.m_nShit);
        //SetSize(fNewSize);

        // 如果被吃的球身上有刺，则刺也被吃过来 poppin test
        //float fCurThornSize = GetThornSize ();
		//float fAddedThornSize = ballOpponent.GetThornSize ();
		//SetThornSize ( CyberTreeMath.CalculateNewSize( fCurThornSize, fAddedThornSize, Main.s_Instance.m_nShit ) );

        ballOpponent.DestroyBall();
	}

	int WhoIsBig( Ball ball1, Ball ball2 )
	{
		if (ball1._Trigger.bounds.size.x == ball2._Trigger.bounds.size.x) {
			return 0;
		} else if (ball1._Trigger.bounds.size.x > ball2._Trigger.bounds.size.x) {
			return 1;
		} else {
			return -1;
		}
	}

	bool CheckIfTotallyCover( CircleCollider2D circle1, CircleCollider2D circle2  )
	{
		float r1 = circle1.bounds.size.x / 2.0f;
		float r2 = circle2.bounds.size.x / 2.0f;

		float deltaX = circle1.bounds.center.x - circle2.bounds.center.x;
		float deltaY = circle1.bounds.center.y - circle2.bounds.center.y;
		float dis = Mathf.Sqrt ( deltaX * deltaX + deltaY * deltaY );
		if (dis <= Mathf.Abs (r1 - r2)) {
			return true;
		}

		return false;
	}

	public bool CheckIfCanForceSpit()
	{
		return true;
	}

	public bool CheckIfCanSpitBall( float fMotherSize, float fChildSize, ref float fMotherLeftSize  )
	{
		fMotherLeftSize= CyberTreeMath.CalculateNewSize ( fMotherSize, fChildSize, Main.s_Instance.m_nShit, -1 );
		return fMotherLeftSize > Main.BALL_MIN_SIZE;
	}

	// 

	public Spore SpitSpore()
	{
		float fMotherSize = GetSize();
		float fMotherLeftSize = 0.0f;
		if (!CheckIfCanSpitBall (fMotherSize, Main.BALL_MIN_SIZE, ref fMotherLeftSize)) {
			return null;
		}
		Main.s_Instance.s_Params [0] = Main.eInstantiateType.Spore;
		Main.s_Instance.s_Params [1] = 444;
		GameObject goSpore = Main.s_Instance.PhotonInstantiate (Main.s_Instance.m_preSpore, Vector3.zero, Main.s_Instance.s_Params );//GameObject.Instantiate( Main.s_Instance.m_preSpore );
		Spore spore = goSpore.GetComponent<Spore>();
		goSpore.transform.parent = Main.s_Instance.m_goSpores.transform;
		spore._srMain.color = this._srMain.color;
		this.CalculateNewBallBornPosAndRunDire ( spore, this._dirx, this._diry );
		spore.BeginEject ( Main.s_Instance.m_fSpitSporeInitSpeed, Main.s_Instance.m_fSpitSporeAccelerate );

		SetSize ( fMotherLeftSize );

		return spore;
	}


	public void SpitNail()
	{
		photonView.RPC ( "RPC_SpitNail", PhotonTargets.AllBuffered );
	}

	[PunRPC]
	public void RPC_SpitNail()
	{
		float fMotherSize = GetSize();
		float fMotherLeftSize = 0.0f;
		if (!CheckIfCanSpitBall (fMotherSize, Main.BALL_MIN_SIZE, ref fMotherLeftSize)) {
			return;
		}

		Nail nail = null;

		GameObject goNail = GameObject.Instantiate (Main.s_Instance.m_preNail); // “钉子”是网络版的哦
		goNail.transform.parent = Main.s_Instance.m_goNails.transform;
		nail = goNail.GetComponent<Nail> ();
		float fChildPosX = 0.0f, fChildPosY = 0.0f;
		float fMotherPosX = 0.0f, fMotherPosY = 0.0f;
		float fMotherDirX = 0.0f, fMotherDirY = 0.0f;
		this.GetPos ( ref fMotherPosX, ref fMotherPosY );
		this.GetDir ( ref fMotherDirX, ref fMotherDirY );
		float fMotherRadius = this._srMain.bounds.size.x / 2.0f;
		float fChildRadius = nail._srMain.bounds.size.x / 2.0f;
		CyberTreeMath.CalculateSpittedObjBornPos (fMotherPosX, fMotherPosY, fMotherRadius, fMotherDirX, fMotherDirY, fChildRadius, ref fChildPosX, ref fChildPosY);
		nail.SetPos ( fChildPosX, fChildPosY );
		nail.SetDir ( fMotherDirX, fMotherDirY );
		float fRunSpeed = 0.0f;
		float fRunAccelerate = 0.0f;
		CyberTreeMath.CalculateSpittedObjRunParams ( Main.s_Instance.m_fSpittedObjrunRunDistance, Main.s_Instance.m_fSpittedObjrunRunTime ,ref fRunSpeed, ref fRunAccelerate );
		nail.BeginEject ( fRunSpeed, fRunAccelerate );

		if (photonView.isMine) {
			SetSize (fMotherLeftSize);
		} else {
			Local_SetSize (fMotherLeftSize  );
		}
	}

	public float GetBoundsSize()
	{
		return _srMain.bounds.size.x;
	}

	public void GetDir( ref float fDirX, ref float fDirY )
	{
		fDirX = _dirx;
		fDirY = _diry;
	}

	public Vector3 GetPos()
	{
		return this.gameObject.transform.position;
	}

	public void GetPos( ref float fX, ref float fY )
	{
		fX = this.gameObject.transform.position.x;
		fY = this.gameObject.transform.position.y;
	}

	public void SetPos( float fX, float fY )
	{
		photonView.RPC ( "RPC_SetPos", PhotonTargets.All, fX, fY );
	}

	[PunRPC]
	public void RPC_SetPos( float fX, float fY )
	{
		Local_SetPos ( fX, fY );
	}

	public void Local_SetPos( float fX, float fY )
	{
		vecTempPos = this.gameObject.transform.position;
		vecTempPos.x = fX;
		vecTempPos.y = fY;
		this.gameObject.transform.position = vecTempPos;
	}

	public void SetDir( float fX, float fY )
	{
		photonView.RPC ( "RPC_SetDir", PhotonTargets.All, fX, fY );
	}

	[PunRPC]
	public void RPC_SetDir( float fX, float fY )
	{
		Local_SetDir(  fX,  fY );
	}

	public void Local_SetDir(  float fX, float fY  )
	{
		this._dirx = fX;
		this._diry = fY;
	}

	// 计算新吐出的球（包括刺）初始位置和冲刺方向
	public void CalculateNewBallBornPosAndRunDire( Ball new_ball, float fDirX, float fDirY )
	{
		vecTempPos = GetPos();

		float fMotherCurRadiusAbsVal = _srMain.bounds.size.x  / 2.0f;
		float fChildCurRadiusAbsVal =  new_ball._srMain.bounds.size.x / 2.0f;
		float fDis = fMotherCurRadiusAbsVal +  fChildCurRadiusAbsVal;

		//new_ball._dirx = fDirX;
		//new_ball._diry = fDirY;
		new_ball.SetDir( fDirX, fDirY );

		vecTempPos.x += fDis * new_ball._dirx;
		vecTempPos.y += fDis * new_ball._diry;
        CyberTreeMath.ClampBallPosWithinSCreen(vecTempPos, ref vecTempPos2);
		//new_ball.gameObject.transform.position = vecTempPos;
		new_ball.SetPos(vecTempPos2.x, vecTempPos2.y );
	}



	float m_fChildSize = 0.0f;
	float m_fMotherLeftSize = 0.0f;
	float m_fChildThornSize = 0.0f;
	float m_fMotherLeftThornSize = 0.0f;
	public void CalculateChildSize( float fForceSpitPercent )
	{
        /*
		float fChildSize = 0.0f;
		float fMotherSize = GetSize ();
		float fMotherLeftSize = 0.0f; 
		fChildSize = fMotherSize * fForceSpitPercent;
		if (fChildSize < Main.BALL_MIN_SIZE) {
			fChildSize = Main.BALL_MIN_SIZE;
		}

		fMotherLeftSize = fMotherSize - fChildSize;
		if ( fChildSize >= Main.BALL_MIN_SIZE &&  fMotherLeftSize >= Main.BALL_MIN_SIZE && fMotherLeftSize >= fChildSize) {
			m_fChildSize = fChildSize;
			m_fMotherLeftSize = fMotherLeftSize;
		} 
		*/

        // 按面积来算，不能简单粗暴的按直径来算

        float fMotherSize = GetSize ();
		float fMotherThornSize = GetThornSize ();

        /*
		float fRealPercent = CyberTreeMath.AreaToSize (fForceSpitPercent, Main.s_Instance.m_nShit );
		fChildSize = fMotherSize * fRealPercent; 
		m_fChildThornSize = fMotherThornSize * fRealPercent; 
		if (fChildSize < Main.BALL_MIN_SIZE) {
			fChildSize = Main.BALL_MIN_SIZE;
		}
		fMotherLeftSize = CyberTreeMath.CalculateNewSize ( fMotherSize, fChildSize, Main.s_Instance.m_nShit, -1 );
		m_fMotherLeftThornSize = Mathf.Sqrt ( fMotherThornSize * fMotherThornSize - m_fChildThornSize * m_fChildThornSize );
		if (fChildSize >= Main.BALL_MIN_SIZE && fMotherLeftSize >= Main.BALL_MIN_SIZE && fMotherLeftSize >= fChildSize) {
			m_fChildSize = fChildSize;
			m_fMotherLeftSize = fMotherLeftSize;
		}
        */
        // “size”一律指“直径”
        float fMotherArea = CyberTreeMath.SizeToArea(fMotherSize, Main.s_Instance.m_nShit);
        float fChildArea = fForceSpitPercent * fMotherArea;
        float fChildSize = CyberTreeMath.AreaToSize(fChildArea, Main.s_Instance.m_nShit);
        float fMotherLeftArea = fMotherArea - fChildArea;
        float fMotherLeftSize = CyberTreeMath.AreaToSize(fMotherLeftArea, Main.s_Instance.m_nShit);
        m_fChildSize = fChildSize;
        m_fMotherLeftSize = fMotherLeftSize;
        
    }

	public void ClearAvailableChildSize()
	{
		m_fChildSize = 0.0f;
	}

	public float GetAvailableChildSize( ref float fMotherLeftSize )
	{
		fMotherLeftSize = m_fMotherLeftSize;
		return m_fChildSize;
	}

	bool m_bShell = false;
	float m_fShellShrinkCurTime = 0.0f;
	public void BeginShell()
	{
		photonView.RPC ( "RPC_BeginShell", PhotonTargets.All );
	}

	[PunRPC]
	public void RPC_BeginShell()
	{
        Local_BeginShell();


    }

    void Local_BeginShell()
    {
        m_bShell = true;
        _Collider.isTrigger = false;
        vecTempScale.x = 1.1f;
        vecTempScale.y = 1.1f;
        vecTempScale.z = 1.0f;
        //_shell.transform.localScale = vecTempScale;
        SetShellSize(vecTempScale);
        m_fShellShrinkCurTime = 0.0f;
        cTempColor = _srShellForAlpha.color;
        cTempColor.a = 0.4f;
        _srShellForAlpha.color = cTempColor;
    }

	void Shell()
	{
		if (_shell == null) {
			return;
		}

		if (!m_bShell) {
			return;
		}

		m_fShellShrinkCurTime += Time.deltaTime;
		float fLeftSize = 1.1f - 0.1f * m_fShellShrinkCurTime / Main.s_Instance.m_fShellShrinkTotalTime;
		vecTempScale = _shell.transform.localScale;
		vecTempScale.x = fLeftSize;
		if (vecTempScale.x < 1.0f) {
			EndShell ();
		}

		vecTempScale.y = vecTempScale.x;
		//_shell.transform.localScale = vecTempScale;
		SetShellSize(vecTempScale);
	}

	public void EndShell()
	{
		m_bShell = false;
        m_fShellShrinkCurTime = 0.0f;

        vecTempScale.x = 0.9f;
		vecTempScale.y = 0.9f;
		vecTempScale.z = 0.9f;
		//_shell.transform.localScale = vecTempScale;
		SetShellSize(vecTempScale);

		cTempColor = _srShellForAlpha.color;
		cTempColor.a = 0.0f;
		_srShellForAlpha.color = cTempColor;

		_Collider.isTrigger = true;
	}

	void SetShellSize( Vector3 vecScale )
	{
		_shell.transform.localScale = vecScale;
		_shellForAlpha.transform.localScale = vecScale;
	}

	public float m_fSpitBallInitSpeed = 5.0f;           // 分球的初速度
	public float m_fSpitBallAccelerate = 0.0f;         // 吐孢子的加速度(这个值通过距离、初速度、时间 算出来，不用配置) 

	public float m_fExplodeInitSpeed = 0.0f;
	public float m_fExplodeInitAccelerate = 0.0f;

	SpitBallTarget m_SpitBallTarget = null;
	public void ShowTarget()
	{
		/*
		if (m_SpitBallTarget == null) {
			m_SpitBallTarget = (GameObject.Instantiate (Main.s_Instance.m_preSpitBallTarget)).GetComponent<SpitBallTarget>();
			m_SpitBallTarget.gameObject.transform.parent = Main.s_Instance.m_goSpitBallTargets.transform;
		}

		float fMotherLeftSize = 0.0f;
		float fChildSize = GetAvailableChildSize ( ref fMotherLeftSize );
		if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE  || fMotherLeftSize < fChildSize) {
			m_SpitBallTarget.gameObject.SetActive (false);
			return;
		} else {
			m_SpitBallTarget.gameObject.SetActive (true);
		}

		vecTempScale.x = fChildSize;
		vecTempScale.y = fChildSize;
		vecTempScale.z = 1.0f;
		m_SpitBallTarget.gameObject.transform.localScale = vecTempScale;

		float fRadiusChild = m_SpitBallTarget.GetBoundsSize() / 2.0f;
		float fRadiusMother = GetBoundsSize( ) * fMotherLeftSize / GetSize () / 2.0f;
		vecTempPos = this.gameObject.transform.position;
		float v0 = Main.s_Instance.m_fBallEjectBaseSpeed;
		float a = Main.s_Instance.m_fBallEjectAccelerate;
		float fDis = v0 * v0 / (2 * a);
		fDis += fRadiusChild;
		fDis += fRadiusMother;
		float fTargetPosX = vecTempPos.x + fDis * _dirx;
		float fTargetPosY = vecTempPos.y + fDis * _diry;
		vecTempPos.x = fTargetPosX;
		vecTempPos.y = fTargetPosY;
		m_SpitBallTarget.gameObject.transform.position = vecTempPos;
		*/
		if (m_SpitBallTarget == null) {
			m_SpitBallTarget = (GameObject.Instantiate (Main.s_Instance.m_preSpitBallTarget)).GetComponent<SpitBallTarget>();
			m_SpitBallTarget.gameObject.transform.parent = Main.s_Instance.m_goSpitBallTargets.transform;
		}

		float fMotherSize = GetSize ();
		float fRadiusMother= GetBoundsSize () / 2.0f;

		float fMotherLeftSize = 0.0f;
		float fChildSize = GetAvailableChildSize ( ref fMotherLeftSize );
		if (fChildSize < Main.BALL_MIN_SIZE || fMotherLeftSize < Main.BALL_MIN_SIZE  || fMotherLeftSize < fChildSize) {
			m_SpitBallTarget.gameObject.SetActive (false);
			return;
		} else {
			m_SpitBallTarget.gameObject.SetActive (true);
		}

		vecTempScale.x = fChildSize;
		vecTempScale.y = fChildSize;
		vecTempScale.z = 1.0f;
		m_SpitBallTarget.gameObject.transform.localScale = vecTempScale;
		float fRadiusChild = m_SpitBallTarget.GetBoundsSize() / 2.0f;
		/*
		m_fSpitBallRunDistance = ( Main.s_Instance.m_fSpitBallRunDistanceMultiple - 1 ) * fRadiusMother - fRadiusChild ;
		m_fSpitBallInitSpeed = Main.GetV0 (m_fSpitBallRunDistance, Main.s_Instance.m_fSpitRunTime  );
		m_fSpitBallAccelerate = Main.GetA ( m_fSpitBallRunDistance, Main.s_Instance.m_fSpitRunTime);
		*/
		CalculateSpitBallRunParams ( fRadiusMother, fRadiusChild, Main.s_Instance.m_fSpitBallRunDistanceMultiple, ref m_fSpitBallInitSpeed, ref m_fSpitBallAccelerate );
		vecTempPos = this.gameObject.transform.position;
		vecTempPos.x += ( Main.s_Instance.m_fSpitBallRunDistanceMultiple * fRadiusMother ) * _dirx ;
		vecTempPos.y += ( Main.s_Instance.m_fSpitBallRunDistanceMultiple * fRadiusMother ) * _diry ;
		vecTempPos.z = 0;
		m_SpitBallTarget.gameObject.transform.localPosition = vecTempPos;
	}

	public void CalculateSpitBallRunParams( float fRadiusMother, float fRadiusChild, float fMultiple, ref float fInitSpeed, ref float fAccelerate )
	{
		float fRunDistance = (fMultiple - 1) * fRadiusMother;
		fInitSpeed = CyberTreeMath.GetV0 (fRunDistance, Main.s_Instance.m_fSpitRunTime  );
		fAccelerate = CyberTreeMath.GetA ( fRunDistance, Main.s_Instance.m_fSpitRunTime);
	}

	public SpitBallTarget _relateTarget = null;
	public void RelateTarget( Ball ball )
	{
		ball._relateTarget = m_SpitBallTarget;
	}

	public void RefeshPosDueToOutOfScreen()
	{
/*		
		if (_balltype == eBallType.ball_type_thorn && _isSpited) {
			return;
		}
*/
		if (!CheckIfOutOfScreen ()) {
			return;
		}

		this.gameObject.transform.position = Main.s_Instance.RandomPosWithinScreen ();
	}

	public bool CheckIfOutOfScreen()
	{
		Vector3 vecScreenPos = Camera.main.WorldToScreenPoint ( this.gameObject.transform.position );

		if (vecScreenPos.x < 0 || vecScreenPos.x > Screen.width || vecScreenPos.y < 0 || vecScreenPos.y > Screen.height) {
			return true;
		}

		return false;
	}

    public void AutoAttenuateThorn()
    {
        float fThornAttenuate = 0.0f;
        if (MapEditor.s_Instance)
        {
            fThornAttenuate = MapEditor.s_Instance.GetThornAttenuateSpeed();
        }
        float fCurThornSize = GetThornSize();
        fCurThornSize -= fCurThornSize * fThornAttenuate * Time.deltaTime;
        if (fCurThornSize < 0)
        {
            fCurThornSize = 0;
        }
        Local_SetThornSize(fCurThornSize);

    }


    public void AutoAttenuate( float val )
	{
		float fCurSize = GetSize ();
		if (fCurSize <= Main.BALL_MIN_SIZE) {
			return;
		}

		fCurSize -= fCurSize * val;
		if (fCurSize < Main.BALL_MIN_SIZE) {
			fCurSize = Main.BALL_MIN_SIZE;
		}
		Local_SetSize (  fCurSize );



   
    }

	void ShowSize()
	{
		if (_text) {
			_text.text = GetSize ().ToString ("f2");
		}
	}

    public void Terminate()
    {
        DestroyBall();
    }

    public void EatGrassSeed( int nGrassGUID )
    {
        photonView.RPC("RPC_EatGrassSeed", PhotonTargets.AllBuffered, this.photonView.ownerId, nGrassGUID, PhotonNetwork.time );
    }

    [PunRPC]
    public void RPC_EatGrassSeed( int nOwnerId, int nGrassGUID, double dCurTime )
    {
        Local_EatGrassSeed(nOwnerId, nGrassGUID, dCurTime);
    }

    public void Local_EatGrassSeed(int nOwnerId, int nGrassGUID, double dCurTime)
    {
        MapEditor.s_Instance.ProcessGrassSeed(nOwnerId, nGrassGUID, dCurTime);
    }

}