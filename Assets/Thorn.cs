﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thorn : MapObj //  “刺”继承于“球”，尼玛刺就是球的癌细胞
{
	static List<float> m_lstNewThornApplication = new List<float> ();

    public GameObject _main;
    public CircleCollider2D _Trigger;
    public SpriteRenderer _srMain;

    public static string[] c_aryThornColor =
    {
        "#FFFFFFFF",
        "#FF0000FF",
        "#F6850DFF",
        "#F6F900FF",
        "#70F900FF",
        "#00F9B1FF",
        "#0035F9FF",
        "#7400F6FF",
    };

    public struct ColorThornParam
    {
        public float thorn_size;
        public float ball_size;
    };

    int m_nColorIdx = 0;

    // Use this for initialization
    void Start () {
		
	}

	void Awake()
	{
	

	}

	// Update is called once per frame
	public override void Update () {
        base.Update();

	}


	void FixedUpdate()
    {

	}

	public static void ApplyGenerateOneNewThorn ()
	{
		m_lstNewThornApplication.Add ( Main.s_Instance.m_fGenerateNewThornTimeInterval );

	}

	public static void GenerateNewThornLoop()
	{
		for ( int i = 0; i < m_lstNewThornApplication.Count; i++ ) {
			float fTimeLeft = m_lstNewThornApplication [i];
			fTimeLeft -= Time.deltaTime;
			m_lstNewThornApplication [i] = fTimeLeft;
			if (fTimeLeft <= 0.0f) {
				Main.s_Instance.GenerateThorn ();
				m_lstNewThornApplication.RemoveAt (i);
				break;
			}

		}
	}

    public void SetColorIdx(int nColorIdx )
    {
        if (nColorIdx < 0 || nColorIdx >= c_aryThornColor.Length)
        {
            return;
        }
        m_nColorIdx = nColorIdx;
        _srMain.color = GetColorByIdx(m_nColorIdx);
    }

    public int GetColorIdx()
    {
        return m_nColorIdx;
    }

    public static string GetColorStringByIdx(int nColorIdx)
    {
        if (nColorIdx < 0 || nColorIdx >= c_aryThornColor.Length)
        {
            return c_aryThornColor[0];
        }
        return c_aryThornColor[nColorIdx];
    }

    public static Color GetColorByIdx(int nColorIdx)
    {
        Color color = Color.white;
        if (ColorUtility.TryParseHtmlString(GetColorStringByIdx(nColorIdx), out color))
        {
        }
        return color;
    }
}
