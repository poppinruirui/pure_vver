﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberFrame : MonoBehaviour {

    public LineRenderer _lrLeft;
    public LineRenderer _lrRight;
    public LineRenderer _lrTop;
    public LineRenderer _lrBottom;

    Vector3 vecTempPos = new Vector3();

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateFramePos( float fLeft, float fRight, float fTop, float fBottom )
    {


        vecTempPos.x = fLeft + MiniMap.s_Instance.transform.position.x;
        vecTempPos.y = fTop + MiniMap.s_Instance.transform.position.y;
        vecTempPos.z = MiniMap.s_Instance.m_fShit;
        _lrLeft.SetPosition(0, vecTempPos);
        vecTempPos.x = fLeft + MiniMap.s_Instance.transform.position.x; ;
        vecTempPos.y = fBottom + MiniMap.s_Instance.transform.position.y;
        vecTempPos.z = MiniMap.s_Instance.m_fShit;
        _lrLeft.SetPosition(1, vecTempPos);

        vecTempPos.x = fRight + MiniMap.s_Instance.transform.position.x; ;
        vecTempPos.y = fTop + MiniMap.s_Instance.transform.position.y;
        vecTempPos.z = 0.0f;
        _lrRight.SetPosition(0, vecTempPos);
        vecTempPos.x = fRight + MiniMap.s_Instance.transform.position.x; ;
        vecTempPos.y = fBottom + MiniMap.s_Instance.transform.position.y;
        vecTempPos.z = 0.0f;
        _lrRight.SetPosition(1, vecTempPos);

        vecTempPos.x = fLeft + MiniMap.s_Instance.transform.position.x; ;
        vecTempPos.y = fTop + MiniMap.s_Instance.transform.position.y;
        vecTempPos.z = 0.0f;
        _lrTop.SetPosition(0, vecTempPos);
        vecTempPos.x = fRight + MiniMap.s_Instance.transform.position.x; ;
        vecTempPos.y = fTop + MiniMap.s_Instance.transform.position.y;
        vecTempPos.z = 0.0f;
        _lrTop.SetPosition(1, vecTempPos);

        vecTempPos.x = fLeft + MiniMap.s_Instance.transform.position.x; ;
        vecTempPos.y = fBottom + MiniMap.s_Instance.transform.position.y;
        vecTempPos.z = 0.0f;
        _lrBottom.SetPosition(0, vecTempPos);
        vecTempPos.x = fRight + MiniMap.s_Instance.transform.position.x; ;
        vecTempPos.y = fBottom + MiniMap.s_Instance.transform.position.y;
        vecTempPos.z = 0.0f;
        _lrBottom.SetPosition(1, vecTempPos);
    }
}
