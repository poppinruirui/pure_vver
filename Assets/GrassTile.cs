﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassTile : MonoBehaviour {

    Color cTempColor = new Color();

    public SpriteRenderer _srMain;

    List<GrassTile> m_lstNeighbors = new List<GrassTile>(); // 相邻的草皮
    List<GrassTile> m_lstTemp = new List<GrassTile>();
    int m_nX = 0; // 在地图上的格子编号
    int m_nY = 0;

    int m_nStatus = 0;

    // Use this for initialization
    void Start() {

    }

    void Awake()
    {

    }

    // Update is called once per frame
    void Update() {
        AutoRecoverAlpha();

    }

    // 草皮消亡
    public void Die()
    {
        UnBindNeighbor();

        GameObject.Destroy(this.gameObject);
    }

    // 侦测并绑定相邻的草皮
    public void BindNeighbor()
    {
        // 按“九宫格”来检索。一个草皮周围的8个草皮都是它的相邻
        /*
		 *             7     0     1
		 *             6   self    2
		 *             5     4     3
		 */
        int nX = 0, nY = 0;
        for (int i = 0; i < 8; i++)
        {
            switch (i)
            {
                case 0:
                    {
                        nX = m_nX;
                        nY = m_nY + 1;
                    }
                    break;
                case 1:
                    {
                        nX = m_nX + 1;
                        nY = m_nY + 1;
                    }
                    break;
                case 2:
                    {
                        nX = m_nX + 1;
                        nY = m_nY;
                    }
                    break;
                case 3:
                    {
                        nX = m_nX + 1;
                        nY = m_nY - 1;
                    }
                    break;
                case 4:
                    {
                        nX = m_nX;
                        nY = m_nY - 1;
                    }
                    break;
                case 5:
                    {
                        nX = m_nX - 1;
                        nY = m_nY - 1;
                    }
                    break;
                case 6:
                    {
                        nX = m_nX - 1;
                        nY = m_nY;
                    }
                    break;
                case 7:
                    {
                        nX = m_nX - 1;
                        nY = m_nY + 1;
                    }
                    break;

            } // end switch

            GrassTile grass = MapEditor.s_Instance.GetGrass(nX, nY);
            if (grass == null) {
                continue;
            }
            this.AddNeighbor(grass);
            grass.AddNeighbor(this);

        } // end foer
    }

    // 解绑相邻的草皮
    void UnBindNeighbor()
    {

    }

    public void SetIndex(int nX, int nY)
    {
        m_nX = nX;
        m_nY = nY;
    }

    public void GetIndex(ref int nX, ref int nY)
    {
        nX = m_nX;
        nY = m_nY;
    }

    public void AddNeighbor(GrassTile grass)
    {
        m_lstNeighbors.Add(grass);
    }

    public void RemoveNeighbor(GrassTile grass)
    {
        m_lstNeighbors.Remove(grass);
    }

    /*
	 * 这是一个递归函数，不但本草皮要执行，跟它相关联的所有临近草皮都要执行
	 */
    public void SetAlpha(float fAlpha)
    {
        SetAlpha_NoneRecursive(fAlpha);

        GrassTile grass = null;
        string key = m_nX + "," + m_nY;
        if (MapEditor.s_dicTemp.TryGetValue(key, out grass)) {
            return;
        }
        int nX = 0, nY = 0;
        GetIndex(ref nX, ref nY);
        key = nX + "," + nY;
        MapEditor.s_dicTemp[key] = this;

        for (int i = 0; i < m_lstNeighbors.Count; i++) {
            grass = m_lstNeighbors[i];
            if (grass == null) {
                continue;
            }
            grass.SetAlpha(fAlpha);
        }
    }

    float GetAlpha()
    {
        return _srMain.color.a;
    }

    public void SetAlpha_NoneRecursive(float fAlpha)
    {
        cTempColor = _srMain.color;
        cTempColor.a = fAlpha;
        _srMain.color = cTempColor;
    }

    void AutoRecoverAlpha()
    {
        if ( m_nStatus != 0 )
        {
            return;
        }

        float fAlpha = GetAlpha();
        if (fAlpha >= 1.0f)
        {
            return;
        }
        fAlpha += 0.4f * Time.deltaTime;
        if (fAlpha >= 1.0f)
        {
            fAlpha = 1.0f;
        }

        SetAlpha_NoneRecursive(fAlpha);
    }

    public void SetStatus( int nStatus )
    {
        if (m_nStatus == nStatus)
        {
            return;

        }

        SetStatus_NoneRecursive(nStatus);
        if (nStatus == 1)
        {
            SetAlpha_NoneRecursive( 0.4f );
        }
        GrassTile grass = null;
        string key = m_nX + "," + m_nY;
        if (MapEditor.s_dicTemp.TryGetValue(key, out grass))
        {
            return;
        }
        int nX = 0, nY = 0;
        GetIndex(ref nX, ref nY);
        key = nX + "," + nY;
        MapEditor.s_dicTemp[key] = this;

        for (int i = 0; i < m_lstNeighbors.Count; i++)
        {
            grass = m_lstNeighbors[i];
            if (grass == null)
            {
                continue;
            }
            grass.SetStatus(nStatus);
        }
    }

    public void SetStatus_NoneRecursive(int nStatus)
    {
        m_nStatus = nStatus;
    }
}
