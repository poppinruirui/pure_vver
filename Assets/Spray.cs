﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spray : MapObj {

	float m_fRotationZ = 0.0f;

    float m_fSprayInterval = 0.4f;
    float m_fDensity = 5.0f;
    float m_fMaxDis = 2.0f;
    float m_fBeanLifeTime = 1.0f;

	public GameObject _goMain;
    public GameObject _goSprayedBeansContainer;

	Vector3 vecTempRotation = new Vector3();
    Vector3 vecTempPos = new Vector3();

    int m_nBeanCountForthornChance = 0;
    int m_nThornChance = 0;

    public int m_nGUID;




	// Use this for initialization
	void Start () {
		
	}

	void Awake()
	{
        Init();
	}

    void OnMouseDown()
    {
        if (MapEditor.s_Instance)
        {
            MapEditor.s_Instance.PickObj(this);
        }
    }

    void Init()
    {
        m_eObjType = eMapObjType.bean_spray;

        this.gameObject.name = "Spray_" + m_nGUID;
        m_fSprayInterval = 0.1f;
    }

    // Update is called once per frame
    void Update () {
		Rotate ();
		SprayBean ();
	}

	void Rotate()
	{
		float fRotation = Time.deltaTime;
		m_fRotationZ += fRotation;
        if (m_fRotationZ > 360.0f)
        {
            m_fRotationZ = 0.0f;
        }
       // _goMain.transform.localRotation = Quaternion.identity;
       // _goMain.transform.Rotate (0.0f, 0.0f, m_fRotationZ);
	}

	float m_fSprayTimeCount = 0.0f;
	static int s_Shit = 0;
    List<Bean> m_lstBeans = new List<Bean>();
    List<Thorn> m_lstThorns = new List<Thorn>();
	void SprayBean()
	{
        GameObject goBean = null;
        Bean bean = null;
        if ( Launcher.m_eSceneMode == Launcher.eSceneMode.Game ) // 游戏模式
        { 
            //if (!PhotonNetwork.isMasterClient)
            //{
            //    return;
            //}
        }
        else // 地图编辑器模式
        {
            if ( MapEditor.s_Instance && !MapEditor.s_Instance.m_bShowRealtimeBeanSpray)
            {
               for ( int i = m_lstBeans.Count - 1; i >= 0; i-- )
                {
                    Bean node = m_lstBeans[i];
                    if ( node != null )
                    {
                        GameObject.Destroy( node.gameObject );
                    }
                }

               for ( int i = m_lstThorns.Count - 1; i >= 0; i-- )
                {
                    Thorn node = m_lstThorns[i];
                    if ( node )
                    {
                        GameObject.Destroy(node.gameObject);
                    }
                }

                return;
            }
        }


        if (m_fSprayTimeCount < m_fSprayInterval) {
			m_fSprayTimeCount += Time.deltaTime;
			return;
		}
        int nNum = (int)(m_fSprayTimeCount / m_fSprayInterval);
        if (nNum < 1)
        {
            nNum = 1;
        }
        m_fSprayTimeCount = 0.0f;
        for ( int i = 0; i < nNum; i++ )
        {
            GenerateOneBean();
        }




        
	}

    void GenerateOneBean()
    {
        GameObject goBean = null;
        /*
        if (Launcher.m_eSceneMode == Launcher.eSceneMode.Game) // 游戏模式
        {
            Main.s_Instance.s_Params[0] = Main.eInstantiateType.Bean;
            Main.s_Instance.s_Params[1] = Bean.eBeanType.spray;
            Main.s_Instance.s_Params[2] = 1;
           goBean = Main.s_Instance.PhotonInstantiate(Main.s_Instance.m_preBean, _goMain.transform.position, Main.s_Instance.s_Params);//GameObject.Instantiate( Main.s_Instance.m_preBean );
        }
        */
        //else // 地图编辑器模式
        //{
            goBean = GameObject.Instantiate((GameObject)Resources.Load("preBean"));
            m_lstBeans.Add(goBean.GetComponent<Bean>());
        //}

        vecTempPos = _goMain.transform.position;
        vecTempPos.z -= 1.0f;
        goBean.transform.position = vecTempPos;
        goBean.transform.parent = this.gameObject.transform;
        Bean bean = goBean.GetComponent<Bean>();
        bean.SetLiveTime(m_fBeanLifeTime);
        bean._beanType = Bean.eBeanType.spray;

        float s = (float)UnityEngine.Random.Range(1.0f, m_fMaxDis);
        float t = 2.0f;
        float v0 = CyberTreeMath.GetV0(s, t);
        float a = CyberTreeMath.GetA(s, t);
        //if (Launcher.m_eSceneMode == Launcher.eSceneMode.Game) // 游戏模式
        //{
        //    bean.BeginSpray(v0, a, m_fRotationZ);
        //}
        //else// 地图编辑器模式
        //{

            bean.Local_BeginSpray(v0, a, m_fRotationZ);
        //}

        m_nBeanCountForthornChance++;
        GenerateThorn();
    }

    void GenerateThorn()
    {
        if (m_nThornChance <= 0 )
        {
            return;
        }

        if ( m_nBeanCountForthornChance < m_nThornChance )
        {
            return;
        }
        m_nBeanCountForthornChance = 0;

        GameObject goThorn = null;
        goThorn = GameObject.Instantiate((GameObject)Resources.Load("hedge"));
        vecTempPos = _goMain.transform.position;
        vecTempPos.z -= 1.0f;
        goThorn.transform.position = vecTempPos;
        goThorn.transform.parent = this.gameObject.transform;
        Thorn thorn = goThorn.GetComponent<Thorn>();
        m_lstThorns.Add( thorn );
        thorn.SetLiveTime(m_fBeanLifeTime); // 为简化设计，暂时让喷泉中的豆子和刺采用同样的生命周期
        thorn.m_eObjType = eMapObjType.thorn;

        float s = (float)UnityEngine.Random.Range(1.0f, m_fMaxDis);// 为简化设计，暂时让喷泉中的豆子和刺采用同样的射程范围
        float t = 2.0f;
        float v0 = CyberTreeMath.GetV0(s, t);
        float a = CyberTreeMath.GetA(s, t);
        thorn.Local_BeginMove(v0, a, m_fRotationZ);
    }

    public void SetDensity( float fDensity )
    {
        m_fDensity = fDensity;

        // 通过密度系数算出喷射间隔时间
        m_fSprayInterval = 1.0f / m_fDensity;
    }

    public float GetDensity()
    {
        return m_fDensity;
    }

    public void SetMaxDis( float fMaxDis )
    {
        m_fMaxDis = fMaxDis;
    }

    public float GetMaxDis()
    {
        return m_fMaxDis;
    }

    public void SetBeanLifeTime( float fBeanLifeTime )
    {
        m_fBeanLifeTime = fBeanLifeTime;
    }

    public float GetBeanLifeTime()
    {
        return m_fBeanLifeTime;
    }

    public void SetThornChance( int nThornChance )
    {
        m_nThornChance = nThornChance;
    }

    public int GetThornChance()
    {
        return m_nThornChance;
    }

}
