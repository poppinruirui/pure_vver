﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bean :  Ball {

	static List<float> m_lstNewBeanApplication = new List<float> ();


    float m_fLiveTime = 0.0f;

	public enum eBeanType
	{
		scene,		          // 场景上的最普通的豆子
		spray,                // 喷泉喷出的豆子
	};


	public eBeanType _beanType = eBeanType.scene;
		

	// Use this for initialization
	void Start ()
	{
		
	}

	void Awake()
	{
		_balltype = eBallType.ball_type_bean;

		Init ();
	}

	public override void  Init ()
	{
        int nSprayID = 0;
        if (photonView.instantiationData != null)
        {
            if (photonView.instantiationData.Length >= 2)
            {
                _beanType = (eBeanType)photonView.instantiationData[1];
            }
            if (photonView.instantiationData.Length >= 3)
            {
                nSprayID = (int)photonView.instantiationData[2];
            }
        }
		Transform transMain = this.gameObject.transform.FindChild ("Main");    
		if (transMain) {
			_main = transMain.gameObject;
		} else {
			Debug.LogError ( "transMain empty" );
			return;
		}

		_srMain = _main.GetComponent<SpriteRenderer> ();
		_srMain.color = ColorPalette.RandomFoodColor ();
		_Trigger = _main.GetComponent<CircleCollider2D> ();

		if (_beanType == eBeanType.scene) {
			this.transform.transform.parent = Main.s_Instance.m_goBeans.transform;
		} else if (_beanType == eBeanType.spray ) {
           
        }

      //  m_fLiveTime = Main.s_Instance.m_fSprayBeanLiveTime;


    }

    public void SetLiveTime( float fLiveTime )
    {
        m_fLiveTime = fLiveTime;
    }
	
	// Update is called once per frame
	public override void Update () {
		Spraying ();
        LiveTimeCounting();

    }

	public override void FixedUpdate()
	{

	}

	public static void ApplyGenerateOneNewBean ()
	{
		m_lstNewBeanApplication.Add ( Main.s_Instance.m_fGenerateNewBeanTimeInterval );
	
	}

	public static void GenerateNewBeanLoop()
	{
		for ( int i = 0; i < m_lstNewBeanApplication.Count; i++ ) {
			float fTimeLeft = m_lstNewBeanApplication [i];
			fTimeLeft -= Time.deltaTime;
			m_lstNewBeanApplication [i] = fTimeLeft;
			if (fTimeLeft <= 0.0f) {
				Main.s_Instance.GenerateBean ();
				m_lstNewBeanApplication.RemoveAt (i);
				break;
			}

		}
	}

	float _speedX = 0.0f;
	float _speedY = 0.0f;
	bool m_bSpraying = false;
	public void BeginSpray( float fInitSpeed, float fAccelerate, float fDirection )
	{
		photonView.RPC ( "RPC_BeginSpray", PhotonTargets.AllBuffered, fInitSpeed, fAccelerate, fDirection );
	}

	[PunRPC]
	public void RPC_BeginSpray( float fInitSpeed, float fAccelerate, float fDirection )
	{
		Local_BeginSpray (fInitSpeed, fAccelerate, fDirection);
	}

	float m_fInitSpeed = 0.0f;
	float m_fAccelerate = 0.0f;
    float m_fDirection = 1.0f;
	public void Local_BeginSpray( float fInitSpeed, float fAccelerate, float fDirection )
	{
		m_fInitSpeed = fInitSpeed;
		m_fAccelerate = fAccelerate;
        m_fDirection = fDirection;
        m_bSpraying = true;
	}

	public void Spraying()
	{
		if (!m_bSpraying) {
			return;
		}
        //Debug.Log(m_fInitSpeed);
		vecTempPos = this.transform.localPosition;
        _speedX = Mathf.Sin(m_fDirection) * m_fInitSpeed;
        _speedY = Mathf.Cos(m_fDirection) * m_fInitSpeed;
        vecTempPos.x += _speedX * Time.deltaTime;
		vecTempPos.y += _speedY * Time.deltaTime;
		this.transform.localPosition = vecTempPos;
		m_fInitSpeed += m_fAccelerate * Time.deltaTime;
		if (m_fInitSpeed <= 0.0f) {
			EndSpray ();
		}
	}

	public void EndSpray()
	{
		m_bSpraying = false;
	}
    void LiveTimeCounting()
    {
        if (_beanType != eBeanType.spray)
        {
            return;
        }

        m_fLiveTime -= Time.deltaTime;
        if ( m_fLiveTime <= 0.0f )
        {
            Ball.DestroyBean( this );
        }
    }
    
}
